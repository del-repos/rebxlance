[![Go](https://github.com/AOrps/rebxlance/actions/workflows/go.yml/badge.svg)](https://github.com/AOrps/rebxlance/actions/workflows/go.yml)
[![Go Report Card](https://goreportcard.com/badge/github.com/AOrps/rebxlance)](https://goreportcard.com/report/github.com/AOrps/rebxlance)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Maintainability](https://api.codeclimate.com/v1/badges/4e5372c279e42457ecfb/maintainability)](https://codeclimate.com/github/AOrps/rebxlance/maintainability)
# rebxlance
Rebalance Portfolio

## To Prepare for use:
```sh
# Getting the repo
git clone https://github.com/AOrps/rebxlance.git

# Changing Directory to repo
cd rebxlance/
```

##  To Run:
```sh
# For Debug Mode (No Compilation)
go run main.go

# To Compile
go build main.go
```

## To Use: 

### Disclaimer:
- This is not to be a replacement or a substitute for investment advice,

## Limitations
